# Sender-Kafka



## Dependencies

Before starting the service, it is necessary to execute the following commands
to download the required dependencies:

```
github.com/confluentinc/confluent-kafka-go/kafka
```

## Running code

Code can be run with the following command:

```
go run main.go
```