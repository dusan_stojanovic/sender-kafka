package kafkaproducer

import (
	"log"

	"github.com/confluentinc/confluent-kafka-go/kafka"
)

type KafkaProducer struct {
	producer *kafka.Producer
}

func InitKafkaProducer() *KafkaProducer {
	conf := &kafka.ConfigMap{
		"bootstrap.servers": "localhost",
	}

	producer, err := kafka.NewProducer(conf)
	if err != nil {
		log.Fatalf("Kafka producer error: %v", err)
	}

	return &KafkaProducer{
		producer: producer,
	}
}

func (kp *KafkaProducer) SendMessage(key string) error {
	topic := "messages"

	topicPartition := kafka.TopicPartition{
		Topic:     &topic,
		Partition: kafka.PartitionAny,
	}

	message := &kafka.Message{
		TopicPartition: topicPartition,
		Key:            []byte(key),
		Value:          []byte("Hello World!!!"),
	}

	return kp.producer.Produce(message, nil)
}

func (kp *KafkaProducer) Close() {
	kp.producer.Close()
}
