package main

import (
	"fmt"
	"log"
	"sender-kafka/kafkaproducer"
	"strconv"
)

func main() {
	log.Println("Starting Sender Service")
	producer := kafkaproducer.InitKafkaProducer()

	defer producer.Close()

	for i := 0; i < 10; i++ {
		err := producer.SendMessage(strconv.FormatInt(int64(i), 10))
		if err != nil {
			fmt.Println("Error: ", err)
		}
	}

	fmt.Println("All messages are sent !!!")
}
